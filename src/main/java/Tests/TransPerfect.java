package Tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class TransPerfect {

    public static void main(String[] args) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://eu.wahoofitness.com/");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, 15);

        driver.findElement(By.xpath("//*[@id=\"top_nav\"]/ul/li[1]/a")).click();

        driver.findElement(By.xpath("//*[@id=\"section-4\"]/div/div/ul/li[3]/div[2]/div[4]/form/button")).sendKeys(Keys.PAGE_DOWN); //Scrolls a bit down to a product to be added to cart

        driver.findElement(By.xpath("//*[@id=\"section-4\"]/div/div/ul/li[7]/div[2]/div[3]/form/button")).click(); // Add product to cart

        Thread.sleep(2000);
        try {
            assert driver.findElement(By.xpath("//*[@id=\"#header\"]/div[4]")).isDisplayed(); // Assert if side-bar cart appears when adding product
             } catch (Exception e) {
            System.out.println("Side-Bar cart not displayed after adding product to the cart. \n Error: " + e);
        }

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"top-cart-btn-checkout\"]")));
        driver.findElement(By.xpath("//*[@id=\"top-cart-btn-checkout\"]")).click(); // go to cart

        Thread.sleep(5000);
        try {
            assert driver.getCurrentUrl().equals("https://eu.wahoofitness.com/checkout/");
        } catch (Exception e){
            System.out.println("User could not get to the checkout page \n Error: " + e);
        }

        driver.navigate().back();



        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"side-nav\"]/div/div[2]/ul/li[5]/a")));
        driver.findElement(By.xpath("//*[@id=\"side-nav\"]/div/div[2]/ul/li[5]/a")).sendKeys(Keys.PAGE_DOWN);
        driver.findElement(By.xpath("//*[@id=\"side-nav\"]/div/div[2]/ul/li[5]/a")).sendKeys(Keys.PAGE_DOWN);
        driver.findElement(By.xpath("//*[@id=\"section-4\"]/div/div/ul/li[30]/div[2]/div[3]/form/button")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"#header\"]/div[4]")));


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mini-cart\"]/li/div/div/div[3]/div/a")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mini-cart\"]/li[1]/div/div/div[3]/div/a")));
        driver.findElement(By.xpath("//*[@id=\"mini-cart\"]/li[1]/div/div/div[3]/div/a")).click(); // remove first product

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[11]/aside[2]/div[2]/footer/button[2]")));
        driver.findElement(By.xpath("/html/body/div[11]/aside[2]/div[2]/footer/button[2]")).click();

        try{
            assert driver.getPageSource().contains("Item was removed successfully");
        } catch (Exception e){
            System.out.println("No message that the item has successfully been remove is displayed \n Error: " + e);
        }

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"minicart-content-wrapper\"]/div[2]/div[4]/div/a")));
        driver.findElement(By.xpath("//*[@id=\"minicart-content-wrapper\"]/div[2]/div[4]/div/a")).click(); // view and edit cart

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[4]/div[1]/ul/li[1]/button"))); // proceed to checkout
        try {
            assert driver.getCurrentUrl().equals("https://eu.wahoofitness.com/checkout/cart/");
        } catch (Exception e){
            System.out.println("User was not sent on the correct url. \nError: " + e);
        }

driver.findElement(By.className("input-text qty")).sendKeys("9"); //Change quantity of product
        Thread.sleep(5000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[4]/div[1]/ul/li[1]/button"))); // proceed to checkout
        driver.findElement(By.xpath("//button[@title=\"Proceed to Checkout\"]")).click(); // go to checkout



        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@title=\" Place Order\"]")));
        Thread.sleep(5000);
        driver.findElement(By.xpath("//button[@title=\" Place Order\"]")).click();
        try{
            assert driver.getPageSource().contains("This is a required field.");
        } catch (Exception e) {
            System.out.println("User could continue without completing shipping address. \nError: " + e);
        }

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"customer-email\"]")));
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"s_method_amstrates_amstrates22\"]")).click(); // shipping method
        driver.findElement(By.name("username")).sendKeys("email@something.com"); // email address
        driver.findElement(By.name("firstname")).sendKeys("Dan"); // first name
        driver.findElement(By.name("lastname")).sendKeys("Stefan"); // last name
        driver.findElement(By.name("street[0]")).sendKeys("Sergent Maresal Averescu"); // Street address
        driver.findElement(By.name("city")).sendKeys("Berlin"); // city
        driver.findElement(By.name("postcode")).sendKeys("1234"); // postal code
        driver.findElement(By.name("telephone")).sendKeys("4123412334"); // phone number

        Thread.sleep(3000);
        driver.findElement(By.name("cardnumber")).sendKeys("123123123123123"); //credit card number
        driver.findElement(By.name("exp-date")).sendKeys("1221"); //exp date cvc
        driver.findElement(By.name("cvc")).sendKeys("213");
        driver.findElement(By.xpath("//button[@title=\" Place Order\"]")).click(); // place order


        try {
            assert driver.getPageSource().contains("Please verify you card information.");
        } catch (Exception e) {
            System.out.println("Please verify you card information. \nError: " + e);
        }





        driver.close();
        driver.quit();
    }
}
