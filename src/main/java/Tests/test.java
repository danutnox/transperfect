package Tests;

import Pages.WahooFitness;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class test {
    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://eu.wahoofitness.com/");
        WahooFitness.shopCategories(driver).click();
        System.out.println(WahooFitness.products(driver));
    }
}
